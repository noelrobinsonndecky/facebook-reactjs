import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App";
import "./Styles/style.css";

const maRoot = ReactDOM.createRoot(document.getElementById("maRoot"));
maRoot.render(<App />);
