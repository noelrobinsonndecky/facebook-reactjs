import React from "react";
import Login from "./composants/Login";
import Logo from "./composants/Logo";

const App = () => {
  return (
    <div className="container p-3 ">
      <div className="container m-auto row">
        <div className="col-12 col-md-6">
          <Logo />
        </div>
        <div className="col-12 col-md-6">
          <Login />
        </div>
      </div>
    </div>
  );
};

export default App;
