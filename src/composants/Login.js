import React from "react";

const Login = () => {
  return (
    <div className="col-12 p-1">
      <div className="card p-3">
        <form action="get">
          <input
            className="w-100 p-2"
            type="text"
            placeholder="Adresse e-mail ou numéro de Tel"
          />
          <br />
          <input
            className="p2 w-100 p-2 mt-4"
            type="password"
            placeholder="Mot de passe"
          />
          <div>
            <button className="btn connect w-100 p-2 mt-4">
              <strong className="text-light">Se connecter</strong>
            </button>
            <p className="text-center mt-4">
              <a href="www.facebook.com/login">Mot de passe oublié ?</a>
            </p>
          </div>
          <hr />
          <div className="text-center">
            <button className="btn ccompte w-75 p-2 mt-2 inscription">
              Créer nouveau compte
            </button>
          </div>
        </form>
      </div>
      <p className="text-center mt-4">
        <strong>Créer une page</strong> pour une célébrité, une marque ou une
        entreprise.
      </p>
    </div>
  );
};

export default Login;
