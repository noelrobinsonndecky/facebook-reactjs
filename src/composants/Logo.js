import React from "react";

const Logo = () => {
  return (
    <div className="col-12 p-1 logo">
      <div>
        <h1 className="face">facebook</h1>
        <p className="monP">
          Avec facebook, partagez et restez en <br /> contact avec votre
          entourage.
        </p>
      </div>
    </div>
  );
};

export default Logo;
